import avro.schema
import io
import datetime
import random

#from avro.datafile import DataFileWriter
#from avro.io import DatumWriter, BinaryEncoder
#from io import BytesIO
from kafka import KafkaProducer
from random import randint
from threading import Thread
from time import sleep

def call_at_interval(period, callback, args):
    while True:
        sleep(period)
        callback(*args)

def setInterval(period, callback, *args):
    Thread(target=call_at_interval, args=(period, callback, args)).start()

def dataStream():

	#random_last_three = randint(0, 99)
#	random_last_three_norek = '0' + str(random_last_three) if len(str(random_last_three)) == 1 else str(random_last_three)
#	norek = '121000' + random_last_three_norek

    SOURCE_FILE = "Arlita"
    NAMA_FITUR = ""
    NAMA_KATEGORI = ['Tunai', 'Non Tunai']

    DATA_GRUP = {
        'Tarik Tunai' : ['103010020', '103010030', '103010040', '103010050', '103010060', '103010070', '103010080', '103010090', '103010100'],
        'Transfer BRI' : ['205010360', '205010361', '205031660', '205010400', '205010430'],
        'Cek Saldo' : ['201010170', '201010171', '201010172', '201010173', '201010180', '201010190', '201010200', '201010210', '201010220', '201010230'],
        'Transfer Bersama' : ['205010370', '205010380', '205010390', '205010420', '205010440', '205011420', '205031650', '205031670', '205011420'],
        'Transfer BRIVA' : ['206290011']
    }
    NAMA_GRUP = random.choice(list(DATA_GRUP.keys()))
    kode_grup = random.choice(DATA_GRUP[NAMA_GRUP])

    NAMA_SUB_GRUP = ""
    DATA_REGION = {"01":["BANDUNG"], "02":["JAKARTA"]}
    KODE_REGION = random.choice(list(DATA_REGION.keys()))
    NAMA_REGION = random.choice(DATA_REGION[KODE_REGION])

    DESKRIPSI_REGION = ""
#12=ASIA AFRIKA, 23=RIAU, 34=AHMAD YANI
    DATA_CABANG = { 'ASIA AFRIKA':['12'], 'RIAU':['23'], 'AHMAD YANI':['34']}
    KODE_CABANG = random.choice(list(DATA_CABANG.keys()))
    NAMA_CABANG = random.choice(DATA_CABANG[KODE_CABANG])

    DATA_REKENING = { 'Azmira':['121000001'], 'Nita': ['121000002'], 'Alifar': ['121000003'], 'Boby': ['121000004'], 'Jada':['121000005'], 'Hendra': ['121000007'], 'Mia':['121000008']}
    nama_penerima = random.choice(list(DATA_REKENING.keys()))
    REKENING_PENERIMA = random.choice(DATA_REKENING[nama_penerima])
    REKENING_SUMBER = "121000006"

    KODE_BANK_PENERIMA = ""

    SISA_UANG = ""

    JUMLAH_TRX = ['100000', '200000', '300000', '500000', '1000000']

    TANGGAL_TRX = datetime.datetime.now().strftime('%Y-%m-%d')
    WAKTU = datetime.datetime.now()
    TAHUN =  WAKTU.year
    BULAN = WAKTU.month
    HARI = WAKTU.day
    JAM = WAKTU.hour
    MENIT = WAKTU.minute
    DETIK = WAKTU.second

    WAKTU_TRX = (JAM*3600) + (MENIT*60) + DETIK

    def kodeRespon():
            data_kode_respon = ['51', '61']
            kode_respon_random = randint(1, 100)
            if kode_respon_random <= 95:
                kode_respon = '00'
            else:
                kode_respon = random.choice(data_kode_respon)
                return kode_respon

    def kodeMataUang():
    		kode_mata_uang_random = randint(1, 100)
    		if kode_mata_uang_random <= 95:
    			kode_mata_uang = '360'
    		else:
    			kode_mata_uang = '361'
    		return kode_mata_uang


#    tipe_channel = ['ATM', 'EDC', 'INTERNET BANKING', 'SMS BANKING']
#	jenis_transaksi = ['TarikTunai', 'TransferBRI', 'TransferBRIVA', 'TransferBersama']
#	status_code = ['00', '51']
#	amount = 50000 * randint(1, 100)
#	timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')#.timestamp()

    result = {
        'NAMA_FITUR' : NAMA_FITUR,
        'NAMA_KATEGORI' : NAMA_KATEGORI[randint(0, 1)],
        'KODE_CABANG' : KODE_CABANG,
        'NAMA_CABANG' : NAMA_CABANG,
        'NAMA_PENERIMA' : nama_penerima,
        'REKENING_PENERIMA' : REKENING_PENERIMA,
        'KODE_BANK_PENERIMA' : "",
        'NAMA_GRUP' : NAMA_GRUP,
        'NAMA_SUB_GRUP' : "",
        'KODE_REGION' : KODE_REGION,
        'NAMA_REGION' : NAMA_REGION,
        'NAMA_UNIT' : "",
        'KETERANGAN_TRX' : "",
        'NAMA_PENGIRIM' : "Arlita",
        'BANK_PENGIRIM' : "BRI",
        'REKENING_SUMBER' : "121000006",
        'TIPE_REKENING_SUMBER' : "",
        'ID_FITUR' : kode_grup,
        'KODE_MATA_UANG' : kodeMataUang(),
        'NO_KARTU' : '5432' + REKENING_SUMBER,
        'JUMLAH_TRX' : JUMLAH_TRX[randint(0, 4)],
        'FEE_TRANSAKSI' : 0,
        'KODE_PROC' : 12000,
        'KODE_RESPON' : kodeRespon(),
        'KODE_RESPONSE_DESC' : "",
        'TANGGAL_TRX' : TANGGAL_TRX,
        'ID_TERMINAL' : "",
        'TIPE_TERMINAL' : "",
        'WAKTU_TRX' : WAKTU_TRX,
    }

    #print(result)
	#	ENCODE DATA3 AS BINARY DATA


#	print(raw_bytes)

#___________________33_______MAIN__________________________
data = result

bytes_writer = io.BytesIO()
encoder = avro.io.BinaryEncoder(bytes_writer)
writer.write(data, encoder)

raw_bytes = bytes_writer.getvalue()

#print(raw_bytes)

#	SEND BINARY DATA TO PRODUCED KAFKA TOPIC "test_producer"
producer = KafkaProducer(bootstrap_servers='192.168.2.248:9092')
producer.send('arlita',raw_bytes)

if	__name__ == '__main__':

	##PARAMETERS
	producer_schema = avro.schema.Parse(open("test_producer.avsc").read())
	writer = avro.io.DatumWriter(producer_schema)